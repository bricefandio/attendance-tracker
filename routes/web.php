<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('home');
});*/
Route::get('/add_dept', function(){return view('add_dept');});
Route::get('/view_dept', function(){return view('view_dept');});
Route::get('/add_employe', function(){return view('add_employe');});
Route::get('/view_employe', function(){return view('view_employe');});
Route::get('/home', function(){return view('home');});


Route::get('/authentification', 'pagescontroller@auth');
Route::get('/footer', 'pagescontroller@footer');

Route::get('/save_department', 'DepartmentController@save_department');
Route::get('/save_employe', 'EmployeController@save_employe');

Auth::routes();


