<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Employe extends Model {
    protected $fillable = [
        'matricule', 'departement_id', 'nom', 'prenom', 'phone_number', 'email_address', 'date_of_birth', 'sexe', 'date_embauche'  //ce fichier est la BD.nom et chef sont ecrits exactement comme dans la BD xampp fillable veut dire lié ie pour lié a la BD 
    ];
}