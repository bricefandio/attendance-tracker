@extends('layouts.master')

@section('title')
Add New Department
@endsection

@section('page_name')
Add Department
@endsection

@section('content')

<div class="container">
  
  <div class="alert alert-success">
    <strong>Success!</strong> This alert box could indicate a successful or positive action.
  </div>
</div>


<!--<div class="container">
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Great!</strong> New department registered successfully.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
</div> -->


<form action="/save_department" method="GET">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName1">Nom</label>
                    <input name="nom" class="form-control" type="text" placeholder="Enter Name" required>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputName1">Chef</label>
                    <input name=" chef" class="form-control" type="text" placeholder="Enter the name of chief department">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div> 

                @csrf
              </form>
@endsection